# Welcome to MITx's 6.00.1x! #

# Introduction to Computer Science and Programming Using Python.
### A new and updated introduction to computer science as a tool to solve real-world analytical problems using Python 3.5.


```

https://www.edx.org/course/introduction-computer-science-mitx-6-00-1x-10

https://courses.edx.org/courses/course-v1:MITx+6.00.1x+2T2017/courseware/


```


### Resources to Help you Succeed

```
Official Python 3 Documentation - https://docs.python.org/3/library/index.html

Dive Into Python - http://www.diveintopython3.net/

Think Python 2e - http://greenteapress.com/wp/think-python-2e/

The Python Tutorial - https://docs.python.org/3/tutorial/

Learn Python the Hard Way - https://learnpythonthehardway.org/book/

Reserved Keywords in Python - https://docs.python.org/3.0/reference/lexical_analysis.html#id8

PEP 8 -- Style Guide for Python Code - https://www.python.org/dev/peps/pep-0008/

Invent with Python - https://inventwithpython.com/

Codecademy - https://www.codecademy.com/learn/python

Python Tutor - http://www.pythontutor.com/

DiffChecker - https://www.diffchecker.com/

Debugging in Python - https://pythonconquerstheuniverse.wordpress.com/2009/09/10/debugging-in-python/

Numbers_and_operators - http://techearth.net/python/index.php5?title=Python:Basics:Numbers_and_operators

Using Python as a Calculator - https://docs.python.org/3/tutorial/introduction.html


```







 
* Length:  9 weeks
* Effort:  15 hours per week
* Institution:  MITx
* Subject:  Computer Science
* Level:  Introductory
* Languages:  English
