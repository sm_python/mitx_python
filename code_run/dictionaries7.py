def fib(n):
    global numFibCalls
    numFibCalls += 1
    if n == 1:
        return 1
    elif n == 2:
        return 2
    else:
        return fib(n - 1) + fib(n - 2)


def fibEf(n, dictionary):
    global numFibCalls
    numFibCalls += 1


    if n in dictionary:
        return dictionary[n]
    else:
        ans = fibEf(n - 1, dictionary) + fibEf(n - 2, dictionary)
        dictionary[n] = ans
        return ans


numFibCalls = 0
fibArg = 30

print(fib(fibArg))
print('function calls:', numFibCalls)

numFibCalls = 0


dictionary  = {1:1, 2:2}

print(fibEf(fibArg, dictionary))
print('function calls:', numFibCalls)