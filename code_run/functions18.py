def foo(x):
    def bar(z, x=0):
        return z + x

    return bar(3, x)


print(foo(2))
print(foo(1))
print(foo(0))
