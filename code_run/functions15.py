def printName(firstName, lastName, reverse = False):
    if reverse:
        print(lastName + ', ' + firstName)
    else:
        print(firstName, lastName)


printName('Alex', 'Under')
printName('Alex', 'Under', False)
printName('Alex', 'Under', True)
