def a(x, y, z):
    if x:
        return y
    else:
        return z


print(a(False, 2, 3))
print(a(True, 2, 3))

print('=======================')


def b(q, r):
    return a(q > r, q, r)


print(b(3, 2))
print(a(True, 3, 2))

print('=======================')

print(a(3 > 2, a, b))

print('=======================')

# print(b(a, b))
