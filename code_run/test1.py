def maxOfThree(a, b, c):
    """
    a, b, and c are numbers

    returns: the maximum of a, b, and c
    """
    if a > b:
        bigger = a

    else:
        bigger = b

    if c > bigger:
        bigger = c

    return bigger


# maxOfThree(1, 2, 3)
print(maxOfThree(1, -1, 100))
print(maxOfThree(2, -10, 100))
print(maxOfThree(7, 9, 10))
print(maxOfThree(6, 1, 5))
print(maxOfThree(0, 40, 20))
