def oddTuples(aTup):
    '''
    aTup: a tuple returns: tuple, every other element of aTup.
    '''
    # a placeholder to gather our response
    rTup = ()
    index = 0

    # Idea: Iterate over the elements in aTup,
    # counting by 2 (every other element) and adding that element to the result
    while index < len(aTup):
        rTup += (aTup[index],)
        index += 2

    return rTup


def oddTuples2(aTup):
    '''
    Another way to solve the problem.   aTup: a tuple   returns: tuple, every other element of aTup.
    '''
    # Here is another solution to the problem that uses tuple slicing by 2 to achieve the same result
    return aTup[::2]


print(oddTuples(()))
print(oddTuples2(()))

print(oddTuples((9,)))
print(oddTuples((1, 17, 6, 16, 2)))
print(oddTuples((1, 15, 8, 4, 6, 19, 10, 5, 17, 7)))
print(oddTuples((17, 15, 13, 2, 8, 12, 7)))
print(oddTuples((1, 3, 0, 2, 17, 8, 0, 5, 12)))
print(oddTuples((7, 8, 10, 4, 12, 8, 1, 0, 9)))
print(oddTuples((10, 11, 10, 6, 10, 0, 18)))
print(oddTuples((3, 15, 1, 13, 11, 13, 19, 0)))
print(oddTuples((14, 12, 13, 2, 18, 18)))
