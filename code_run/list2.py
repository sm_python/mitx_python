'''
LiSTS
https://pythonworld.ru/tipy-dannyx-v-python/spiski-list-funkcii-i-metody-spiskov.html
 aList = [0, 1, 2, 3, 4, 5]
 bList = aList
 aList[2] = 'hello'
 aList == bList

'''

aList = [0, 1, 2, 3, 4, 5]
bList = aList
aList[2] = 'hello'
aList == bList
print(aList == bList)
print('---')

print(aList)
print(bList)
print(aList is bList)
print(aList)

cList = [6, 5, 4, 3, 2]
dList = []
for num in cList:
    dList.append(num)
print(cList == dList)
print('---')

print(cList)
print(dList)

print(cList is dList)

cList[2] = 20
print(cList)
print(dList)
