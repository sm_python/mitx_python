'''
TUPLES
'''

t = (2, 'one', 3)


def get_data(aTuple):
    nums = ()
    words = ()

    for t in aTuple:
        nums = nums + (t[0],)
        if t[1] not in words:
            words = words + (t[1],)
    min_nums = min(nums)
    max_nums = max(nums)
    unique_words = len(words)
    return (min_nums, max_nums, unique_words)


(small, large, words) = get_data(((1, 'mine'), (3, 'yours'), (5, 'ours'), (7, 'mine')))

print(t)
print(t + (5, 6))
print(t[1:2])
print(('one',))
print(('one'))

print(small)
print(large)
print(large)

# print(t[0])
# print(t[1])
# print(t[2])
# print(t[-1])
# print(t[-2])
# print(t[-3])
