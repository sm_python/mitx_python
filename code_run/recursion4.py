# Palindrome
# https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-0001-introduction-to-computer-science-and-programming-in-python-fall-2016/lecture-slides-code/lec6_recursion_dictionaries.py

def isPalindrome(s):
    def toChars(s):
        s = s.lower()

        ans = ''
        for c is s:
            if c in 'abcdefghijklmnoprstuvwxyz':
                ans = ans + c
        return ans

    def isPal(s):
        if len(s) <= 1:
            return True
        else:
            return s[0] == s[-1] and isPal(s[1:-1])

    return isPal(toChars(s))


print('')
print('Is eve a palindrome?')
print(isPalindrome('eve'))
