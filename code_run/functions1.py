# Write a Python function, evalQuadratic(a, b, c, x),
# that returns the value of the quadratic a⋅x2+b⋅x+c.

def evalQuadratic(a, b, c, x):
    '''
    a, b, c: numerical values for the coefficients of a quadratic equation
    x: numerical value at which to evaluate the quadratic.
    '''
    return a * x * x + b * x + c


a = 6.5
b = 3.08
c = -0.08
x = 1.41
z = evalQuadratic(a, b, c, x)
print(z)
