'''

Write an iterative function iterPower(base, exp) that calculates the exponential
baseexp by simply using successive multiplication.
 For example, iterPower(base, exp) should compute baseexp by multiplying base times
  itself exp times. Write such a function below.

This function should take in two values - base can be a float or an integer;
exp will be an integer ≥ 0. It should return one numerical value. Your code must
 be iterative - use of the ** operator is not allowed.

'''


def iterPower(base, exp):
    '''
    base: int or float.
    exp: int >= 0

    returns: int or float, base^exp
    '''

    result = 1

    while exp > 0:
        result *= base
        exp -= 1
    return result


print(iterPower(4, 2))
print(iterPower(7.16, 0))
print(iterPower(-1.47, 10))
print(iterPower(-4.05, 6))
print(iterPower(6.13, 0))
print(iterPower(-1.03, 1))
print(iterPower(-1.26, 0))
print(iterPower(1.67, 1))
print(iterPower(-6.99, 5))
