try:

    a = int(input('Enter a number:  '))
    b = int(input('Enter second number:  '))

    print('a/b = ', a / b)
    print('a+b = ', a + b)

    print('OK!')

except ValueError:
    print('ERROR! - could not convert to the number')

except ZeroDivisionError:
    print('ERROR! - could not divide to zero')

except :
    print('ERROR! - other errors')

print('Done!')
