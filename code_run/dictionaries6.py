def fib(n, dictionary):
    if n in dictionary:
        return dictionary[n]
    else:
        ans = fib(n - 1, dictionary) + fib(n - 2, dictionary)
        dictionary[n] = ans
        return ans


dictionary = {1: 1, 2: 2}

argToUse = 4


print(fib(argToUse, dictionary))
