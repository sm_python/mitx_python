def printName(firstName, lastName, reverse):
    if reverse:
        print(lastName + ', ' + firstName)
    else:
        print(firstName, lastName)


printName('Alex', 'Under', False)
printName('Alex', 'Under', True)
