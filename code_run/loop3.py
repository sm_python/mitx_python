# Write a while loop that sums the values 1 through end, inclusive.
# end is a variable that we define for you.
#  So, for example, if we define end to be 6,
# your code should print out the result: 21

# which is 1 + 2 + 3 + 4 + 5 + 6.




# from tkinter import BOTH, END, LEFT
# tkinter.Listbox.insert(tkinter.END, row)
#
# total = 0
# current = 1
# while current <= end:
#     total += current
#     current += 1
#
# print(total)

# Here is one of a few ways to solve this problem:
total = 0
for next in range(1, end+1):
    total += next
print(total)

# Here is another:
total = end
for next in range(end):
    total += next
print(total)