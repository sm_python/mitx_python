def factorial_iter(n):
    prod = 1
    for i in range(1, n + 1):
        prod *= i
    return prod


print(factorial_iter(4))
print(factorial_iter(3))
print(factorial_iter(2))
print(factorial_iter(1))
print(factorial_iter(-1))
print(factorial_iter(0))
