# Convert the following into code that uses a while loop.

# prints Hello!
# prints 10
# prints 8
# prints 6
# prints 4
# prints 2

num = 10
print("Hello!")

while num > 0:
    print(num)
    num -= 2

# There are always many ways to solve a programming problem. Here is one:
print("Hello!")
num = 10
while num > 0:
    print(num)
    num -= 2

# Here is another:
num = 11
print("Hello!")
while num > 1:
    num -= 1
    if num % 2 == 0:
        print(num)

# There are always many ways to solve a programming problem. Here is one:
print("Hello!")
for num in range(0, 10, 2):
    print(10 - num)

# Here is another:
print("Hello!")
for num in range(10, 0, -2):
    print(num)