def foo(x, y=5):
    def bar(x):
        return x + 1

    return bar(y * 2)


print(foo(3))
print(foo(1))
print(foo(0))
print(foo(3, 5))
print(foo(3, 0))
