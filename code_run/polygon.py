'''
A regular polygon has n number of sides. Each side has length s.

The area of a regular polygon is: 0.25∗n∗s2tan(π/n)
The perimeter of a polygon is: length of the boundary of the polygon

Write a function called polysum that takes 2 arguments, n and s. This function should sum the area and square of the perimeter of the regular polygon. The function returns the sum, rounded to 4 decimal places.
Hints
What to import?

'''
# This function sum the area and square of the perimeter of the regular polygon.
# The function returns the sum, rounded to 4 decimal places.

import math


def polysum(n, s):
    area = 0.25 * n * s ** 2 / math.tan(math.pi / n)
    perimeter = n * s
    return round(area + (perimeter ** 2), 4)


print(polysum(74, 54))
print(polysum(13, 6))
print(polysum(20, 7))
print(polysum(35, 72))
print(polysum(11, 98))
print(polysum(10, 34))
print(polysum(49, 32))
print(polysum(25, 20))
print(polysum(41, 2))
print(polysum(13, 64))
