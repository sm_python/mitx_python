# https://pythonworld.ru/tipy-dannyx-v-python/slovari-dict-funkcii-i-metody-slovarej.html

d = {}

print(d)

d = {'key1': 1, 'key2': 2}

print(d)

d = dict.fromkeys(['key1', 'key2'])

print(d)

d = dict.fromkeys(['key1', 'key2'], 100)

print(d)

# d = {key: key ** 2 for key in range(7)}
# print(d)
#
# d = {key: key * 2 for key in range(7)}
# print(d)
#
# d = {key: key + 2 for key in range(7)}
# print(d)
#
# d = {key: key - 2 for key in range(7)}
# print(d)

print(d['key1'])
print(d['key2'])
# print(d['key3'])

d[4] = 4**2

print(d)

print(d[4])

print(d)