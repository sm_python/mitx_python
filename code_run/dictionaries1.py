'''
DICTIONARIES

Suppose we evaluate the following expressions:

animals = {'a': 'aardvark', 'b': 'baboon', 'c': 'coati'}

animals['d'] = 'donkey'

'''

animals = {'a': 'aardvark', 'b': 'baboon', 'c': 'coati'}
print(animals)
print(len((animals)))

animals['d'] = 'donkey'
print(animals)
print(animals['c'])

# print(animals['donkey'])

print(len((animals)))

animals['a'] = 'anteater'
print(animals['a'])

print(len(animals['a']))

print('baboon' in animals)

print('donkey' in animals.values())

print('b' in animals)

print(animals.keys())

print(len(animals))

del animals['b']

print(len(animals))

print(animals.values())
