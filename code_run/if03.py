# Assume that two variables, varA and varB, are assigned values, either numbers or strings.
# Write a piece of Python code that evaluates varA and varB, and then prints out one of the following messages:
# "string involved" if either varA or varB are strings
# "bigger" if varA is larger than varB
# "equal" if varA is equal to varB
# "smaller" if varA is smaller than varB
# alternative ansvers:   https://stackoverflow.com/questions/32716972/how-to-test-the-type-of-variable

varA = "string"
varB = "string"
varA = 2.5
varB = 3.5

if type(varA) == type("s") or type(varB) == type("s"):
    print("string involved")
elif varA > varB:
    print("bigger")
elif varA == varB:
    print("equal")
elif varA < varB:
    print("smaller")
